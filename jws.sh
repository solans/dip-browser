#!/usr/bin/env bash

################################################
#
# Shell wrapper for JWS program used to start Java WebStart applications.
#
# 1) Creates a local temp directory
# 2) Downloads the latest JWS program version
# 3) Downloads the latest JNLP (if given) application and runs it
#
# Felix Ehm BE-CSS 02/2022
#
################################################

###
# local variables
JWS_EXEC="jws.jar"
JWS_WEB="http://bewww.cern.ch/ap/deployments-dev/applications/cern/devops/deploy/jws/PRO/$JWS_EXEC"
JWS_LOCAL_DIR="${HOME:-/tmp}/.java/deployment/cache/jws"
JWS_LOCAL_EXEC="$JWS_LOCAL_DIR/$JWS_EXEC"

# default JAVA_HOME for controls BE-CSS managed Linux machines
DEFAULT_JAVA_HOME="/usr/java/jdk"

###
# set JAVA_HOME if not done before
if [[ -z $JAVA_HOME ]]; then
  
  # check if default JAVA_HOME exists
  if [[ ! -d $DEFAULT_JAVA_HOME ]];  then
    echo "$(basename $0): ERROR: \$JAVA_HOME variable was not set and the default JAVA_HOME location $DEFAULT_JAVA_HOME does not exist either"
    echo "$(basename $0): ERROR: Please set \$JAVA_HOME variable to valid location and run the script again"
    exit 10
  fi

  # set JAVA_HOME to default location
  echo "$(basename $0): INFO: \$JAVA_HOME variable was not set, setting it to the default location $DEFAULT_JAVA_HOME"
  JAVA_HOME=$DEFAULT_JAVA_HOME
fi

###
# create jws binary directory
/bin/mkdir -p $JWS_LOCAL_DIR || exit 10

###
# download the jws executable before execution

JWS_TEMP=$JWS_LOCAL_EXEC.$$

/usr/bin/wget -q -O $JWS_TEMP $JWS_WEB
if [[ -s $JWS_TEMP ]]; then
  /bin/mv $JWS_TEMP $JWS_LOCAL_EXEC
else
  # wget downloaded an empty file (URL or server error)
  echo "$(basename $0): WARNING: Download of $JWS_WEB was empty, continuing ..."
  /bin/rm -f $JWS_TEMP
fi

# bail out if there was a problem
if [[ ! -f $JWS_LOCAL_EXEC ]];  then
  echo "$(basename $0): ERROR: Could not download jws from $JWS_WEB"
  exit 10
fi

###
# runs the jnlp given as argument
exec -a jws $JAVA_HOME/bin/java -jar $JWS_LOCAL_EXEC $*
