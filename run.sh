#!/bin/bash

if [ ! -d ~/.config/icedtea-web ]; then
#       echo "Creating directory ~/.config/icedtea-web"
        mkdir ~/.config/icedtea-web
fi
if [ -f ~/.config/icedtea-web/deployment.properties ]; then
        echo "~/.config/icedtea-web/deployment.properties exist, this is OK"
else
#       echo "Copying deployment.properties file "
        cp /det/dcs/linuxScripts/icedtea-web/deployment.properties ~/.config/icedtea-web/deployment.properties
fi



/usr/bin/javaws http://dipbrowser.web.cern.ch/dipbrowser/launch.jnlpx
