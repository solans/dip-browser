#!/usr/bin/env python

import os
import sys
import argparse
import urllib.request
from xml.dom import minidom

parser=argparse.ArgumentParser()
parser.add_argument('-u','--update',action='store_true',help='update the release')
parser.add_argument('-p','--path',help='java path. default $JAVA_HOME_LINUX/../bin', default='$JAVA_HOME_LINUX/../bin')
parser.add_argument('jnlpx')
args=parser.parse_args()

jnlpx=args.jnlpx
if "http" in args.jnlpx:
    print("download: %s" % args.jnlpx)
    jnlpx=args.jnlpx.split("/")[-1]
    urllib.request.urlretrieve(args.jnlpx, jnplx)
    pass

doc=minidom.parse(jnlpx)

codebase=doc.getElementsByTagName("jnlp")[0].getAttribute("codebase")
properties=[]

for prop in doc.getElementsByTagName("property"):
    properties.append("-D%s=%s" % (prop.getAttribute("name"),prop.getAttribute("value")))

properties.append("-Djava.library.path=%s"%os.path.abspath("resources"))

if not os.path.exists("resources"):
    os.system("mkdir -p resources")
    pass

classpath=[]
for jar in doc.getElementsByTagName("jar"):
    href=jar.getAttribute("href")
    if not os.path.exists("%s/%s"%("resources",href)) and args.update:
        urllib.request.urlretrieve("%s/%s"%(codebase,href), "%s/%s" % ("resources",href))
    classpath.append("%s/%s"%(os.path.abspath("resources"),href))

for jar in doc.getElementsByTagName("nativelib"):
    if "win" in jar.getAttribute("href"): continue
    href=jar.getAttribute("href")
    if not os.path.exists("%s/%s"%("resources",href)) and args.update:
        urllib.request.urlretrieve("%s/%s"%(codebase,href), "%s/%s" % ("resources",href))
    classpath.append("%s/%s"%(os.path.abspath("resources"),href))
    print("cd %s && %s/jar xf %s" % ("resources",args.path,href))
    os.system("cd %s && %s/jar xf %s" % ("resources",args.path,href))
    pass

mainclass=doc.getElementsByTagName("application-desc")[0].getAttribute("main-class")

cmd="%s/java " % args.path
cmd+=" ".join(properties)
cmd+=" -cp "
cmd+=":".join(classpath)
cmd+=" "
cmd+=mainclass

print(cmd)
os.system(cmd)
